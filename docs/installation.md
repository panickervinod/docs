## Installing Open Restaurant on your server

**Step 1:** Download and extract Open Restaurant from Drupal.org at [http://drupal.org/project/restaurant](http://drupal.org/project/restaurant).

**Step 2:** Point your browser to the Open Restaurant installation.

**Step 3:** Follow the steps provided by the installation wizard.

Two types of installation are available.

1. If you want to select which features to enable for your site, select the **standard** installation (slower but recommended).
2. The **demo** installation (quicker) will install the site exactly as the demo site at http://dev-demo-openrestaurant.pantheon.io.

Note: If your **standard** installation is stuck, try to run it again and uncheck the **Install demo content** checkbox. You can install demo content later when the installation completes (see the section on how to install demo content).

## Installing Open Restaurant on Pantheon.

The quickest way to spin an Open Restaurant website is on Pantheon.

**Step 1:** Visit https://dashboard.pantheon.io/products/openrestaurant/spinup and fill in the form to spin a new Open Restaurant site.

**Step 2:** Once the site is created, you will be redirected to the Pantheon Dashboard. Click on **Visit development site** and follow the steps provided by the wizard to install Open Restaurant.

# Using CSS

To start customizing your site, you need to create a custom theme. A good starting point is to use the Sizzle theme as your base theme and add your custom styles on top of that:

**Step 1:** Download the starter theme here: https://github.com/openrestaurant/sizzle_custom/archive/master.zip

**Step 2:** Extract the starter theme to ~/sites/all/themes~ such that your custom theme is at `/sites/all/themes/sizzle_custom`.

**Step 3:** Go to Appearance and under Sizzle Custom, click Enable and set default.

**Step 4:** You can now add your custom style under `/sites/all/themes/sizzle_custom/css/sizzle_custom.style.css`.

**Note: Remember to disable cache to see your css changes.**

# Using Sass

The Open Restaurant distribution has everything you need to kickstart your restaurant website. It comes with a menu management system, a reservation system, a customizable blog, events management and a responsive theme.

#### [Click here for Open Restaurant 2 documentation](http://docs.open.restaurant/en/2.0.dev1/).

## Features

1. **Menu management** - A powerful management system for creating menus, uploading menu pictures, and categorization. Support for nutrition information, menu types and prices included.
2. **Locations** - Manage multiple restaurant locations from one dashboard. Create unique menus, address and opening hours for each location.
3. **Reservation System** - A simple reservation system and calendar is included.
4. **Events and Calendar** - Create and manage events for your restaurant. View events on a calendar by date, week or month.
5. **Blog** - The distribution comes with a blog/news system that you can easily customize.
6. **In-place page building** - Every page is customizable using a simple drag and drop ui.
7. **Widgets** - Maps, address, opening hours and contact forms.
8. **Responsive theme** - Works on all your devices.
